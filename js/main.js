const listaPokemon = document.querySelector("#listaPokemon");
const botonesHeader = document.querySelectorAll(".btn-header");
//Conexion API
let URL = "https://pokeapi.co/api/v2/pokemon/";


//Se deja la iteracion hasta 2000 en caso de que salgan pokemones de la nueva generacion
for (let i = 1; i <= 2000; i++) {
    fetch(URL + i)
        .then((response) => response.json())
        .then(data => mostrarPokemon(data))
        
}






//Funcion que permite mostrar los pokemones obteniendo sus tipos y estadisticas y hacer el div que dispone para cada uno
function mostrarPokemon(poke) {

    let tipos = poke.types.map((type) => `<p class="${type.type.name} tipo">${type.type.name}</p>`);
    tipos = tipos.join('');

    let pokeId = poke.id.toString();
    if (pokeId.length === 1) {
        pokeId = "00" + pokeId;
    } else if (pokeId.length === 2) {
        pokeId = "0" + pokeId;
    }


    const div = document.createElement("div");
    div.classList.add("pokemon");
    div.innerHTML = `
        <p class="pokemon-id-back">#${pokeId}</p>
        <div class="pokemon-imagen">
            <img src="${poke.sprites.other["official-artwork"].front_default}" alt="${poke.name}">
        </div>
        <div class="pokemon-info">
            <div class="nombre-contenedor">
                <p class="pokemon-id">#${pokeId}</p>
                <h2 class="pokemon-nombre">${poke.name}</h2>
            </div>
            <div class="pokemon-tipos">
                ${tipos}
            </div>
            <div class="pokemon-stats">
    
                <p class="stathp">Hp ${poke["stats"][0]["base_stat"]}</p>
                <p class="statatk">Atk ${poke["stats"][1]["base_stat"]}</p>
                <p class="statdef">Def ${poke["stats"][2]["base_stat"]}</p>
                <p class="statsat">SAt ${poke["stats"][3]["base_stat"]}</p>
                <p class="statsdf">SDf ${poke["stats"][4]["base_stat"]}</p>
                <p class="statspd">SpD ${poke["stats"][5]["base_stat"]}</p>
            </div>
            </div>
        </div>
    `
    ;


    listaPokemon.append(div);
   
}


//Ciclo que permite la distribucion de los pokemones 
botonesHeader.forEach(boton => boton.addEventListener("click", (event) => {
    const botonId = event.currentTarget.id;

    listaPokemon.innerHTML = "";
//Se deja la iteracion hasta 2000 en caso de que salgan pokemones de la nueva generacion
    for (let i = 1; i <= 2000; i++) {
        
        fetch(URL + i)
            .then((response) => response.json())
            .then(data => {

                if(botonId === "ver-todos") {
                    mostrarPokemon(data);
                } else {
                    const tipos = data.types.map(type => type.type.name);
                    if (tipos.some(tipo => tipo.includes(botonId))) {
                        mostrarPokemon(data);
                    }
                }

            })
            
    }
}))