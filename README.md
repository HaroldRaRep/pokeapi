# Pokédex con PokéAPI
El proyecto permite hacer una revisión de los pokemones actuales en la API PokeAPI y permite filtrarlo según los tipos de pokemon
#### Video Guia utilizado
El video está en YouTube, [haciendo clic aquí](https://youtu.be/EmxvMPcIy5c).


#### Instalación del proyecto
1.El proyecto está listo para usar descargandolo directamente desde el repositorio y/o haciendole pull al proyecto 

 

#### Cambios Implementados en el proyecto

1. Se amplia la Lista de pokemon y se permite actualizar hasta las proximas generaciones
2. Se añaden estadisticas de los pokemon con sus respectivos colores y valores
3. Se colocan los colores bases de los tipos
4. Se implementa el uso de bootstrap 


##### Contacto
CarpiCoder Proyecto base
 hola@carpicoder.com
 https://github.com/carpicoder/pokedex


Harold Ramirez Arboleda
haroldra@ufps.edu.co